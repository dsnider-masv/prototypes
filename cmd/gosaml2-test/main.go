package main

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	saml2 "github.com/russellhaering/gosaml2"
	"github.com/russellhaering/gosaml2/types"
	dsig "github.com/russellhaering/goxmldsig"
	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Fatalf("Config file 'config.yaml' not found\n")
		}

		log.Fatalf("Could not read in config. Error: %v", err)
	}

	config := new(SAMLConfig)
	if err := viper.Unmarshal(config); err != nil {
		log.Fatalf("Could not unmarshal config. Error: %v", err)
	}

	if err := config.Validate(); err != nil {
		log.Fatalf("Config validation failed: %v", err)
	}

	samlSP, err := configureSAML(config)
	if err != nil {
		log.Fatalf("Could not configure SAML. Error: %v", err)
	}

	app := &App{
		sp: samlSP,
	}

	mux := http.NewServeMux()
	mux.Handle("/", http.HandlerFunc(app.homeHandler))
	mux.Handle("/saml/acs", http.HandlerFunc(app.handleSAMLACS))
	mux.Handle("/saml/metadata", http.HandlerFunc(app.handleGetMetadata))

	fmt.Println("SP ACS URL", samlSP.AssertionConsumerServiceURL)
	url, _ := samlSP.BuildAuthURL("")
	fmt.Println("AUTH URL", url)

	fmt.Println("Server listening on port 8080")
	http.ListenAndServe(":8080", mux)
}

func configureSAML(conf *SAMLConfig) (*saml2.SAMLServiceProvider, error) {
	res, err := http.Get(conf.MetadataURL)
	if err != nil {
		return nil, err
	}

	rawMetadata, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	_ = res.Body.Close()

	metadata := new(types.EntityDescriptor)
	if err := xml.Unmarshal(rawMetadata, metadata); err != nil {
		return nil, err
	}

	// Load keys
	keyPair, err := tls.LoadX509KeyPair("samltest.cert", "samltest.key")
	if err != nil {
		return nil, err
	}

	keyPair.Leaf, err = x509.ParseCertificate(keyPair.Certificate[0])
	if err != nil {
		return nil, err
	}

	certStore := dsig.MemoryX509CertificateStore{
		Roots: []*x509.Certificate{},
	}

	for _, kd := range metadata.IDPSSODescriptor.KeyDescriptors {
		for idx, xcert := range kd.KeyInfo.X509Data.X509Certificates {
			if xcert.Data == "" {
				return nil, fmt.Errorf("metadata certificate[%d] must not be empty", idx)
			}

			// Sanitize certificate data
			xcert.Data = strings.ReplaceAll(xcert.Data, " ", "")
			xcert.Data = strings.ReplaceAll(xcert.Data, "\n", "")
			xcert.Data = strings.ReplaceAll(xcert.Data, "\t", "")

			certData, err := base64.StdEncoding.DecodeString(xcert.Data)
			if err != nil {
				return nil, fmt.Errorf("could not decode metadata certificate[%d] data: %w", idx, err)
			}

			idpCert, err := x509.ParseCertificate(certData)
			if err != nil {
				return nil, fmt.Errorf("could not parse IDP certificate[%d]: %w", idx, err)
			}

			certStore.Roots = append(certStore.Roots, idpCert)
		}
	}

	var keyStore dsig.X509KeyStore = dsig.TLSCertKeyStore{
		Certificate:                  keyPair.Certificate,
		Leaf:                         keyPair.Leaf,
		PrivateKey:                   keyPair.PrivateKey,
		SupportedSignatureAlgorithms: keyPair.SupportedSignatureAlgorithms,
		OCSPStaple:                   keyPair.OCSPStaple,
		SignedCertificateTimestamps:  keyPair.SignedCertificateTimestamps,
	}

	sp := &saml2.SAMLServiceProvider{
		IdentityProviderSSOURL:      metadata.IDPSSODescriptor.SingleSignOnServices[0].Location,
		IdentityProviderIssuer:      metadata.EntityID,
		ServiceProviderIssuer:       conf.ServiceProviderIssuer,
		AssertionConsumerServiceURL: "http://localhost:8080/saml/acs",
		AudienceURI:                 conf.ServiceProviderIssuer,
		ValidateEncryptionCert:      true,
		IDPCertificateStore:         &certStore,
		SPKeyStore:                  keyStore,
		SignAuthnRequests:           true,
	}

	return sp, nil
}

type App struct {
	sp *saml2.SAMLServiceProvider
}

func (app *App) homeHandler(w http.ResponseWriter, r *http.Request) {
	authURL, err := app.sp.BuildAuthURL("")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Could not build auth URL: %v", err)
		return
	}

	fmt.Fprintf(w, `<html><a href="%s">Log in: %s</a></html>`, authURL, authURL)
}

func (app *App) handleSAMLACS(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Error parsing form: %v", err)
		return
	}

	xmlString, err := base64.StdEncoding.DecodeString(r.Form["SAMLResponse"][0])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Could not decode SAML Response within form")
		return
	}

	os.WriteFile("./saml-response.xml", xmlString, os.ModePerm)

	assertionInfo, err := app.sp.RetrieveAssertionInfo(r.FormValue("SAMLResponse"))
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "RetrieveAssertionInfo: %v", err)
		return
	}

	if assertionInfo.WarningInfo.InvalidTime {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "Invalid time")
		return
	}

	if assertionInfo.WarningInfo.NotInAudience {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "Not in audience")
		return
	}

	// If the above checks all passed, authentication was successful.
	// At this point, we would kick off the required processes to create the user within MASV if needed
	// and then return a JWT consumable by our API.
	//
	// IMPORTANT: This JWT should contain a field indicating whether or not a token is a result of SSO and
	// also include which IDP the user's identity was pulled from.

	fmt.Fprintf(w, "Subject NameID: %s\n", assertionInfo.NameID)
	fmt.Fprintf(w, "VALUES:\n")
	for _, v := range assertionInfo.Values {
		fmt.Fprintf(w, "  - %s : %s\n", v.Name, v.Values[0].Value)
	}
	fmt.Fprintf(w, "\n")
	fmt.Fprintf(w, "\n")
	fmt.Fprintf(w, "Warnings:\n")
	fmt.Fprintf(w, "%+v\n", assertionInfo.WarningInfo)
}

func (app *App) handleGetMetadata(w http.ResponseWriter, r *http.Request) {
	metadata, err := app.sp.Metadata()

	// !!! IMPORTANT
	// Round metadata to the nearest second to be compatible with all IDPs.
	// Some providers don't accept smaller units.
	metadata.ValidUntil = metadata.ValidUntil.Round(time.Second)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Could not get metadata: %v", err)
		return
	}

	rawMetadata, err := xml.MarshalIndent(metadata, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Could not marshal metadata: %v", err)
		return
	}

	w.Header().Set("Content-Type", "application/samlmetadata+xml")
	w.Write(rawMetadata)
}
