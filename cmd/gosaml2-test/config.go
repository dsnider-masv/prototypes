package main

import "errors"

type SAMLConfig struct {
	MetadataURL           string `mapstructure:"saml_metadata_url"`
	ServiceProviderIssuer string `mapstructure:"saml_service_provider_issuer"`
}

func (c *SAMLConfig) Validate() error {
	if c.MetadataURL == "" {
		return errors.New("saml_metadata_url is required")
	}

	if c.ServiceProviderIssuer == "" {
		return errors.New("saml_service_provider_issuer is required")
	}

	return nil
}
